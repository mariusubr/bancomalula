package br.com.bancomalula;

/**
 * 
 * @author Maur�cio Pereira
 *
 */

public class Cliente {

	// atributos
	private String nome;
	private String rg;
	private String cpf;
	private String email;
	private Sexo sexo;
	
	public Cliente(String nome, String rg, String cpf, String email, String senha, Sexo sexo) {
		super();
		this.nome = nome;
		this.rg = rg;
		this.cpf = cpf;
		this.email = email;
		this.sexo = sexo;
	}

	// getters & setters
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Sexo getSexo() {
		return sexo;
	}

	public void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}
	
	
}
